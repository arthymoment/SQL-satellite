package com.cht.commonUtil.sql.util;

import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.net.InetAddress;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import static java.lang.System.out;


/**
 * @author Arthur
 *
 */
public class Widgets {

	public static String errLog2Mail(Exception e,String destMail) {
		return errLog2Mail(e,destMail,"");
	}

    public static String errLog2Mail(Exception e,String destMail,String comment) {
        String mes = errProcess(e);

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "10.160.1.1");

        Session session = Session.getDefaultInstance(prop, null);
        MimeMessage msg = new MimeMessage(session);

        String fromAddr = "SQL-Sattlite@cht.com.tw";	//公司內的Server一定要一個fromAddr，只好就捏造一個
        String fromAddrName = "SQL-Satellite";

        try {
            final InternetAddress from = new InternetAddress(fromAddr);
            from.setPersonal(fromAddrName, "big5");
            msg.setFrom(from);
            final InternetAddress ia = new InternetAddress();
            ia.setAddress(destMail);
            msg.addRecipient(Message.RecipientType.TO, ia);

            final Multipart mp = new MimeMultipart();
            final MimeBodyPart mbpTxt = new MimeBodyPart();
            mbpTxt.setContent(mes+"<p><p>"+comment, "text/html;charset=UTF-8");
            mp.addBodyPart(mbpTxt);

            final String address = InetAddress.getLocalHost().getHostAddress().trim();
            msg.setSubject(address+"SQL執行發生錯誤");
            msg.setContent(mp);
            Transport.send(msg);
        } catch (Exception e2) {
            e2.printStackTrace();
        }


        return mes;
    }
	
	public static Connection getConn(String db) {
		Connection conn = null;

		try {
			final Context ctx = new InitialContext();
			final DataSource ds = (DataSource) ctx.lookup( db );
			conn = ds.getConnection();
		} catch (Exception e) {
			out.println("建立連線時發生錯誤"+e.toString());
		}
		return conn;
	}
	
	
	
	
	
	/**
	 *		將errorMessage 改成我想看的格式字串
	 *		<br>創建時間：2013/9/24 上午10:32:12<br>
	 *		創建人員：郭啟揚 (842297)
	 */
	public static String errProcess (Exception e) {
		String mes = e.toString() + "\n\n";
		for (int i=0; i<e.getStackTrace().length;i++) {
			mes += "  File Name:"+e.getStackTrace()[i].getFileName() + "\n";
			mes += "Line Number:"+e.getStackTrace()[i].getLineNumber() + "\n";
		}
		return mes;
	}
	
	/**
	 *		將日期轉換為"預設的格式"輸出
	 *		<br>創建時間：2013/9/27 上午10:06:07<br>
	 *		創建人員：郭啟揚 (842297)
	 */
	public static String changeTimeFormat(Date dt) {
        if( dt != null ) {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dt);
        } else {
            return "";
        }

	}
	
}