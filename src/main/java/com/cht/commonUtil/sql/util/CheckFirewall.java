package com.cht.commonUtil.sql.util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * Created by Arthur on 2016/7/27.
 */
public class CheckFirewall extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/text");

        String result = "FAIL";

        Socket socket=null;
        String hostIP = request.getParameter("hostIP");
        int hostPort = Integer.parseInt( request.getParameter("hostPort") );

        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(hostIP, hostPort),3000);
            result = "SUCCESS";
        } catch (SocketTimeoutException e) {
            System.out.println("FAIL");
        } finally {
            if(socket.isConnected()) {
                socket.close();
            }
        }
        response.getWriter().write(result);
    }
}
