package com.cht.commonUtil.sql.db;


import com.cht.commonUtil.sql.util.Widgets;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ExecuteSQL extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding( "UTF-8" );
		response.setCharacterEncoding( "UTF-8" );
		response.setContentType("application/json");

		String destMail = request.getParameter("destMail");
		if ( destMail == null ) {
			destMail = "arthymoment@cht.com.tw";
		}
		String ds = request.getParameter("ds");
		String sql = request.getParameter("sp");
		Connection conn = null;
		PreparedStatement ps = null;
		
		String res = "SUCCESS";
		
		try {
			conn = Widgets.getConn(ds);
            conn.setAutoCommit(true);
			ps = conn.prepareStatement(sql);
			res = String.valueOf( ps.executeUpdate() );

		} catch ( Exception e) {
			res = Widgets.errLog2Mail(e, destMail,sql);
		} finally {
			try {
				if ( ps!= null ) { ps.close(); }
				if ( conn!= null ) { conn.close(); }
			} catch (SQLException e) { System.out.println(Widgets.errProcess(e)); }
		}
		
		response.getWriter().write(new Gson().toJson( res ));
	}

}
