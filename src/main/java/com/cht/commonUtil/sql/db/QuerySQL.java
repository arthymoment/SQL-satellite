package com.cht.commonUtil.sql.db;


import com.cht.commonUtil.sql.Structure.QueryResult;
import com.cht.commonUtil.sql.util.Widgets;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuerySQL extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding( "UTF-8" );
		response.setCharacterEncoding( "UTF-8" );
		response.setContentType("application/json");

		String destMail = request.getParameter("destMail");
		if ( destMail == null ) {
			destMail = "arthymoment@cht.com.tw";
		}

        QueryResult queryResult = new QueryResult(); //查詢的結果
        List<Map<String,String>> schemeList = new ArrayList<Map<String, String>>();

		String ds = request.getParameter("ds");
		String sql = request.getParameter("sp");
        final boolean IS_DESCRIBE = sql.toLowerCase().startsWith("describe");
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
        ResultSet columnResultSet = null;
		
		List<String[]> records = new ArrayList<String[]>();
		
		try {
            conn = Widgets.getConn(ds);
            ps = conn.prepareStatement(sql);
            resultSet = ps.executeQuery();

            DatabaseMetaData dbMetaData = conn.getMetaData();
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columnCount = resultSetMetaData.getColumnCount();

            for (int i = 1; i <= columnCount; i++) {
                Map<String, String> schema = new HashMap<String, String>();
                String tableName = resultSetMetaData.getTableName(i);
                String columnName = resultSetMetaData.getColumnName(i);
                String columnType = resultSetMetaData.getColumnTypeName(i);
                String showName = columnName;
                String showNameType = "name";

                schema.put("name", showName);
                schema.put("showNameType", showNameType);
                schema.put("columnType", columnType);

                if( !IS_DESCRIBE ) {
                    columnResultSet = dbMetaData.getColumns(null, null, tableName, null);
                    while ( columnResultSet.next() ) {
                        String column_name = new String(columnResultSet.getBytes("COLUMN_NAME"), "utf-8");
                        if (columnName.equals(column_name)) {
                            String comment = new String(columnResultSet.getBytes("REMARKS"), "utf-8");
                            if( comment.length() !=0 ) {
                                showName = comment;
                                showNameType = "comment";
                            }
                            schema.put("name", showName);
                            schema.put("showNameType", showNameType);
                            schema.put("columnType", columnType);
                        }
                    }
                }

                if( columnResultSet != null ) { columnResultSet.close(); }

                schemeList.add(schema);
            }
            while( resultSet.next() ) {
                String[] recordArray = new String[columnCount];
                if( IS_DESCRIBE ) {
                    for(int i=0; i < columnCount;i++) {
                        recordArray[i] = resultSet.getString(i+1);
                    }
                } else {
                    for(int i=0; i < columnCount;i++) {
                        if( schemeList.get(i).get("columnType") != null  ) {    //可能存在運算的查詢
                            if( schemeList.get(i).get("columnType").contains("INT") ) {
                                recordArray[i] = String.valueOf( resultSet.getInt(i+1) );
                            } else if( schemeList.get(i).get("columnType").startsWith("VARCHAR") ) {
                                recordArray[i] = resultSet.getString(i+1);
                            } else if( schemeList.get(i).get("columnType").startsWith("DATETIME") ) {
                                recordArray[i] = Widgets.changeTimeFormat( resultSet.getTimestamp(i+1) );
                            } else if( schemeList.get(i).get("columnType").startsWith("DECIMAL") ) {
                                recordArray[i] = String.valueOf( resultSet.getBigDecimal(i+1) );
                            } else if( schemeList.get(i).get("columnType").startsWith("FLOAT") ) {
                                recordArray[i] = String.valueOf( resultSet.getDouble(i+1) );
                            } else if( schemeList.get(i).get("columnType").startsWith("DATE") ) {
                                recordArray[i] = Widgets.changeTimeFormat(resultSet.getTimestamp(i + 1));
                            } else if( schemeList.get(i).get("columnType").startsWith("TIMESTAMP") ) {
                                recordArray[i] = Widgets.changeTimeFormat(resultSet.getTimestamp(i + 1));
                            } else if( schemeList.get(i).get("columnType").startsWith("TIMESTAMP") ) {
                                recordArray[i] = Widgets.changeTimeFormat(resultSet.getTimestamp(i + 1));
                            } else if( schemeList.get(i).get("columnType").startsWith("NUMBER") ) {
                                recordArray[i] = String.valueOf( resultSet.getLong(i + 1) );
                            }
                        } else {
                            recordArray[i] = resultSet.getString(i+1);
                        }

                    }
                }
                records.add(recordArray);
            }


            queryResult.setRecords(records);
            queryResult.setScheme( schemeList );
            queryResult.setCount(records.size());
            queryResult.setIsSuccess(true);
		} catch ( Exception e) {
			queryResult.setMsg(Widgets.errLog2Mail(e, destMail));
		} finally {
			try {
                if( columnResultSet != null ) { columnResultSet.close(); }
				if (resultSet != null) {resultSet.close();}
				if (ps!= null) {ps.close();}
				if (conn!= null) {conn.close();}
			} catch (SQLException e) { System.out.println(Widgets.errProcess(e));
            }
            }



		response.getWriter().write( new Gson().toJson(queryResult) );
	}

}
