package com.cht.commonUtil.sql.Structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Arthur on 2015/7/30.
 * 查詢結果的物件
 */
public class QueryResult {
    private String msg = "";
    private boolean isSuccess = false;
    private int count =0;
    private List<String[]> records = new ArrayList<String[]>();
    private List<Map<String,String>> scheme = new ArrayList();

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<String[]> getRecords() {
        return records;
    }

    public void setRecords(List<String[]> records) {
        this.records = records;
    }

    public List<Map<String, String>> getScheme() {
        return scheme;
    }

    public void setScheme(List<Map<String, String>> scheme) {
        this.scheme = scheme;
    }
}
