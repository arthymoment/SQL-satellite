/**	系統常數 **/
var CODING="big5";
var TOKENONE = "PPPP";
var REQTOKEN = ",";

var splitTokenONE = "UUUU";
var splitTokenTWO = "TTTT";
var splitTokenTHREE = "XXXX";
var splitTokenFOUR = "PPPP";

/**	清除錯誤訊息 **/
//使用者執行畫面其它動作，需要將原本顯示的錯誤訊息清楚
function clearErrorMsg() {
	$("#errShortMsg").html(null);
	$("#errLongMsg").html(null);
}

function formatNumber(n) {
	n += "";
	var arr = n.split(".");
	var re = /(\d{1,3})(?=(\d{3})+$)/g;
	return arr[0].replace(re,"$1,") + (arr.length == 2 ? "."+arr[1] : "");
}

/**	錯誤訊息產生 **/
//自行維護的錯誤訊息
function errorMsgShow(shortMsg,longMsg) {
	if ( undefined==shortMsg && undefined==longMsg ) {	//兩個都沒有
		$("#errShortMsg").html("woops!  We've got some problem. Please contact the administrator.");
		$("#errLongMsg").html(null);
	} else if ( undefined!=shortMsg && undefined==longMsg ) {	//只有短訊息
		$("#errShortMsg").html(shortMsg);
		$("#errLongMsg").html(null);
	} else {	//兩個都有
		$("#errShortMsg").html(shortMsg);
		$("#errLongMsg").html(longMsg);
	}
}

/**	錯誤訊息產生 **/
//由系統所丟出的錯誤訊息
function errorAjx(xhr) {
	$("#errShortMsg").html("woops!  We've got some problem. <br>Please check the error message in the bottom of the screen.");
	$("#errLongMsg").html("http error code:"+xhr.status+"<p>"+xhr.responseText);	
}

/**	取得子系統, 三個參數分別是，查詢員工的ID，作用的頁面，子系統下拉bar的ID **/
//指定員工ID，則系統便查詢該ID(empId)，在指定頁面(pageName)下，所能查詢的子系統
function querySubsys(empId, pageName, selectId) {
	
	var res=0;
	$.ajax({type:"post",url:"QuerySubsys",dataType:"json",async:false,
		data:{empId:empId,pageName:$.cookie("pageName")},
		success : function(reses) {
			if ( reses.length > 0 ) {	//有找到可操作的子系統
				insertSelect(selectId, reses);
				res=1;
			} else {	//沒有找到
				$(".editable").prop("disabled",true);
				$("#errShortMsg").html("抱歉，系統比對無符合您操作權限的系統");
			}
		},error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
	return res;
}  

/** 產生可異動模組人員的選項，將包含角色，及人員 **/
function queryPerformer(subsysId,tName) {
	//產生角色列表
	$.ajax({type:"post",url:"QueryRole",dataType:"json",async:false,
		data:{roleTypeId:3},
		success : function(reses) {
			if ( reses.length > 0 ) {
				$("#performerPicker").html("");
				
				var tmp = "<table id='tablePerformerRolePicker'><thead class='tableFirst'><tr><th class='center'><input id='tablePerformerRolePickerChkALL' onclick='chkALL(this);' type='checkbox' /></th><th>#</th><th>角色名稱</th></tr></thead><tbody id='tbodyPerformerRolePicker'></tbody></table>";
				$("#performerPicker").append(tmp);
				
				$.each(reses, function(i,res){
					tmp = "<tr>"+
					  "<td class='center'><input class='tablePerformerRolePickerChk' onclick=\"chk('tablePerformerRolePickerChk');\" type='checkbox' value='"+res.i1+"'/></td>"+	//選擇的checkbox
					  "<td class='idx chkRow'>"+(i+1)+"</td>"+		//流水號
					  "<td class='chkRow'>"+res.s1+"</td>"+	//角色名稱描述
					  "</tr>";
					$("#tbodyPerformerRolePicker").append(tmp);	
					
					//如果原本有選的話，這裡要為它打勾
					if ( $("td[roleId='"+res.i1+"']").html()!=undefined ) {
						$("#tbodyPerformerRolePicker tr:last input[type='checkbox']").prop("checked", true);
					}
				});
				
				//綁定輔助點選
				$(".chkRow").bind("click",function(){
					$(this).parent("tr").children("td:first").children("input[type='checkbox']").click();
				});
				//檢查一下，如果所有子項都有勾，那chkAll也要勾
				chk("tablePerformerRolePickerChk");
				
				refreshUI(1,"tbodyPerformerRolePicker",false);
			}
			
			$.ajax({type:"post",url:"QueryEmp",dataType:"json",async:false,
				data:{subsysId:subsysId},
				success : function(resEmpes) {
					if ( resEmpes.length > 0 ) {
						tmp = "<p><table id='tablePerformerEmpPicker'><thead class='tableFirst'><tr><td class='small left' colspan='3'>-同子系所有成員</td></tr><tr><th class='center'><input id='tablePerformerEmpPickerChkALL' onclick='chkALL(this);' type='checkbox' /></th><th>#</th><th>人員名稱</th></tr></thead><tbody id='tbodyPerformerEmpPicker'></tbody></table>";
						$("#performerPicker").append(tmp);
						
						$.each(resEmpes, function(i,res){
							tmp = "<tr>"+
							  "<td class='center'><input class='tablePerformerEmpPickerChk' onclick=\"chk('tablePerformerEmpPickerChk');\" type='checkbox' value='"+res.s1+"'/></td>"+	//選擇的checkbox
							  "<td class='idx chkRow'>"+(i+1)+"</td>"+		//流水號
							  "<td class='chkRow'>"+res.s2+"</td>"+	//員工名稱
							  "</tr>";
							$("#tbodyPerformerEmpPicker").append(tmp);	
							
							//如果原本有選的話，這裡要為它打勾
							if ( $("td[empId='"+res.s1+"']").html()!=undefined ) {
								$("#tbodyPerformerEmpPicker tr:last input[type='checkbox']").prop("checked", true);
							}
						});
						//綁定輔助點選
						$(".chkRow").bind("click",function(){
							$(this).parent("tr").children("td:first").children("input[type='checkbox']").click();
						});
						//檢查一下，如果所有子項都有勾，那chkAll也要勾
						chk("tablePerformerEmpPickerChk");
						
						refreshUI(1,"tbodyPerformerEmpPicker",false);
					}
				},error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
			});
			
			
			if ( $("#tablePerformerRolePicker").size()!=0 || $("#tablePerformerEmpPicker").size()!=0 ) {
				$("#performerPicker").prop("title","可異動的人員或角色").css("visibility", "visible").dialog({
					resizable:true,modal:true,show:"fold",hide:"fold",width:"250",
					buttons : {
						"確定" : function() {
							$("#tbody"+tName+" tr").remove();
							
							var idx=0;
							$(".tablePerformerRolePickerChk:checked").each(function(i){
								idx++;
								tmp = "<tr class='row"+tName+idx+"'>"+
								"<td class='center'><button onclick=\"removeRow(this,'"+tName+"')\" isRole=true value='"+$(this).val()+"' type='button'>-</button></td>"+
								"<td class='idx'>"+idx+"</td>"+
								"<td>角色</td>"+
								"<td roleId='"+$(this).val()+"'>"+$(this).parent("td").next().next().html()+"</td>"+
								"</tr>";
								
								$("#tbody"+tName).append(tmp);
							});
							
							
							$(".tablePerformerEmpPickerChk:checked").each(function(i){
								idx++;
								tmp = "<tr class='row"+tName+idx+"'>"+
								"<td class='center'><button onclick=\"removeRow(this,'"+tName+"')\" isRole=false value='"+$(this).val()+"' type='button'>-</button></td>"+
								"<td class='idx'>"+idx+"</td>"+
								"<td>員工</td>"+
								"<td empId='"+$(this).val()+"'>"+$(this).parent("td").next().next().html()+"</td>"+
								"</tr>";
								
								$("#tbody"+tName).append(tmp);
							});
							
							refreshUI(1,"tbody"+tName,false);
							
							$(this).dialog("close");
						},"關閉" :function() {
							$(this).dialog("close");
						}
					}
				});
			} else {
				errorMsgShow("角色與人員皆無設定");	
			}
			
			
			
		},error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
	
	//產生人員列表
}


/**
 * 		查詢子系統下的設定檔
 *      @param subsysId
 *		<br>創建時間：2013/10/3 上午9:27:53<br>
 *		創建人員：郭啟揚 (842297)
 */
function queryPfINsubsys(subsysId) {
	$.ajax({type:"post",url:"QueryPf",dataType:"json",async:false,
		data:{subsysId:subsysId},
		success : function(reses) {
			if ( reses.length==0 ) {
				errorMsgShow("子系統"+subsysId+"未有任何設定檔被維護，請洽系統人員");
			} else {
				$("#tbodyPfPicker tr").remove();	//先將舊的記錄刪除
				
				$.each(reses, function(i,res){
					var tmp = "<tr>"+
						  "<td class='center'><input class='tablePfPickerChk' type='radio' name='pf' pfId='"+res.s2+"' value='"+res.s1+"' /></td>"+	//選擇的checkbox
						  "<td class='idx'>"+(i+1)+"</td>"+		//流水號
						  "<td>"+res.s1+"</td>"+				//Profile 名稱
						  "<td class='int'>"+res.s2+"</td>"+	//Profile ID
						  "<td>"+res.s3+"</td>"+				//jenkins 套餐名
					"</tr>";
					$("#tbodyPfPicker").append(tmp);
				});
				refreshUI(1,"tbodyPfPicker",false);
				
				$("#pfsPicker").prop("title","選擇設定檔").css("visibility", "visible").dialog({
					resizable:true,modal:true,show:"fold",hide:"fold",width:"500",
					buttons : {
						"確定" : function() {
							$("#pfId").removeClass("warning").attr("pfId",$("input[name='pf']:checked").attr("pfId") ).val( $("input[name='pf']:checked").val() );
							$(this).dialog("close");
						},"關閉" :function() {
							$(this).dialog("close");
						}
					}
				});
			}
		}, error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
}

/**	查詢子系統下的需求	
 *      @param subsysId
 *      @param tbodySelector	最後選擇的需求要加入的tbody是那一個，它的selector
 *		<br>創建時間：2013/10/9 下午4:13:42<br>
 *		創建人員：郭啟揚 (842297)
 */
function queryReqINsubsys(subsysId,tName,modId) {
	var tmp = "QueryReq";
	if( $.cookie("pageName")=="chkIn.html" ){
		tmp = "QueryReqInCra";
	}
	
	$.ajax({type:"post",url:tmp,dataType:"json",async:false,
		data:{subsysId:subsysId,modId:modId,empId:$.cookie("empId")},
		success : function(reses) {
			if ( reses.length==0 ) {
				errorMsgShow("子系統"+subsysId+"未有任何需求被維護，請洽系統人員");
			} else {
				$("#tbodyReqPicker tr").remove();	//先將舊的記錄刪除
				$("#tableReqPickerChkALL").prop("checked",false);
				
				$.each(reses, function(i,res){
					tmp = "<tr>"+
						  "<td class='center'><input class='tableReqPickerChk' onclick=\"chk('tableReqPickerChk');\" type='checkbox'/></td>"+	//選擇的checkbox
						  "<td class='idx chkRow'>"+(i+1)+"</td>"+		//流水號
						  "<td class='chkRow'>"+res.s1+"</td>"+	//需求描述
						  "<td class='int chkRow'>"+res.s2+"</td>"+	//需求ID
						  "<td class='dt chkRow'>"+res.s3+"</td>"+	//創建日期
						  "<td class='chkRow'>"+res.s4+"</td>"+	//創建人員
						  "</tr>";
					$("#tbodyReqPicker").append(tmp);
					
					//檢查<chkIn.html 中的 >
					
					
					//如果原本有選的話，這裡要為它打勾
					if ( $("#tbody"+tName+" td[reqId='"+res.s2+"']").html()!=undefined ) {
						$("#tbodyReqPicker tr:last input[type='checkbox']").prop("checked", true);
					}
				});
				
				//綁定輔助點選
				$(".chkRow").bind("click",function(){
					$(this).parent("tr").children("td:first").children("input[type='checkbox']").click();
				});
				//檢查一下，如果所有子項都有勾，那chkAll也要勾
				chk("tableReqPickerChk");
				
				refreshUI(1,"tbodyReqPicker",false);
				
				$("#reqsPicker").prop("title","選擇需求").css("visibility", "visible").dialog({
					resizable:true,modal:true,show:"fold",hide:"fold",width:"800",async:false,
					buttons : {
						"確定" : function() {
							/* 將checkbox有勾選的項目，加入列表 */
							$("#tbody"+tName+" tr").remove();	//將原本有選的刪除
							$(".tableReqPickerChk").each(function(i){
								if ($(this).is(":checked")) {
									
									var idx= $("#tbody"+replaceDot(tName)+" > tr").size()+1;
									var obj =$(this).parent().next().next();
								    var tmp = "<tr class='row"+tName+idx+"'>"+
									"<td class='center'><button type='button' onclick=\"removeRow(this,'"+replaceDot(tName)+"')\">-</button></td>"+
									"<td class='idx'>"+idx+"</td>"+
									"<td reqId='"+obj.next().html()+"'>"+ obj.html()+"</td>"+
									"</tr>";
									$("#tbody"+replaceDot(tName) ).append(tmp);
									
									if( $.cookie("pageName")=="chkIn.html" ){
										if ( $("#divTabs").prop("maxLen") == undefined || $("#divTabs").prop("maxLen") < obj.html().length ) {
											$("#divTabs").prop("maxLen",obj.html().length );
										}
									}
								}
							});
							
							
							$("#divTabs").width( $("#divTabs").width()+ $("#divTabs").prop("maxLen") );
							$("#mainDiv").width( 550+ $("#divTabs").width() );
							
							refreshUI(1,"tbody"+replaceDot(tName),false);
							
							$(this).dialog("close");
						},"關閉" :function() {
							$(this).dialog("close");
						}
					}
				});
			}
		}, error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
}

function queryModDownStream(modId) {
	var downStream ="<ul id='"+modId+"'>";
	$.ajax({type:"post",url:"QueryModDownStream",dataType: "json",async:false,
		data:{modId:modId},
		success : function(reses) {
			if ( reses.length >0 ) {
				$.each(reses, function(i,res){
					downStream += "<li id='"+res.downSreamNode+"'>"+$.trim(res.downSreamNode)+"</li>";
				});
				downStream +="</ul>";
			}
		},error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
	
	if (downStream == "<ul>") {
		downStream="";
	}
	return downStream;
}


function queryJobOnJenkins(subsysId,tName) {
	$.ajax({type:"post",url:"QueryJobOnJenkins",dataType:"json",async:false,
		data:{subsysId:subsysId},
		success : function(reses) {
			if ( reses.length==0 ) {
				errorMsgShow("子系統"+subsysId+"未有任何模組被維護，請洽系統人員");
			} else {
				$("#tbodyModsPicker tr").remove();	//先將舊的記錄刪除
				$("#tableModsPickerChkALL").prop("checked",false);
					
				$.each(reses, function(i,res){
					
					var tmp = "<tr>"+
					  "<td class='center'><input class='tableModsPickerChk' onclick=\"chk('tableModsPickerChk');\" type='checkbox'/></td>"+	//選擇的checkbox
					  "<td class='idx chkRow'>"+($("#tbodyModsPicker td.idx").size()+1)+"</td>"+		//流水號
					  "<td class='chkRow'>"+res+"</td>"+	//模組名稱
					  "</tr>";
					$("#tbodyModsPicker").append(tmp);
					
					//如果原本有選的話，這裡要為它打勾
					if ( $("#tbodyMods > tr > td[modUnitId='"+res+"']").html()!=undefined ) {
						$("#tbodyModsPicker tr:last input[type='checkbox']").prop("checked", true);
					}
				});
				
				//綁定輔助點選
				$(".chkRow").bind("click",function(){
					$(this).parent("tr").children("td:first").children("input[type='checkbox']").click();
				});
				//檢查一下，如果所有子項都有勾，那chkAll也要勾
				chk("tableModsPickerChk");
				
				refreshUI(1,"tbodyModsPicker",false);
				
				$("#modsPicker").prop("title","選擇模組").css("visibility", "visible").dialog({
					resizable:true,modal:true,show:"fold",hide:"fold",width:"400",async:false,
					buttons : {
						"確定" : function() {
							/* 將checkbox有勾選的項目，加入列表 */
							$("#tbody"+tName+" tr").remove();	//將原本有選的刪除

							$(".tableModsPickerChk").each(function(i){
								if ($(this).is(":checked")) {
									var idx= $("#tbody"+tName+" > tr").size()+1;
									var obj =$(this).parent().next().next();
									var modId = obj.html();
									var donwStreamMod=queryModDownStream(modId);
								    var tmp = "<tr class='row"+tName+idx+"'>"+
									"<td class='center'><button type='button' class='hideable' onclick=\"removeRow(this,'"+tName+"')\">-</button></td>"+
									"<td class='idx'>"+idx+"</td>"+
									"<td modUnitId='"+modId+"'>"+ obj.html()+"</td>"+
									"<td class='topVer'>"+donwStreamMod+"</td>"+
									"<td class='topVer'><br><table id='table"+modId+"'><thead class='tableFirst'><tr><td><button type='button' class='hideable' id='btnSetDownstream' modId='"+modId+"' onclick='selDownstreamMod(this)'>+</button></td><th class='center'>#</th><th>模組ID</th></tr></thead><tbody id='tbody"+modId+"'></tbody></table><br></td>"+
									"</tr>";
									$("#tbody"+tName).append(tmp);
									
									
									$("ul#"+modId+" li").each(function(i){
										idx= $("#tbody"+modId+" > tr").size()+1;
										tmp = "<tr class='row"+modId+idx+"'>"+
										"<td class='center'><button class='hideable' type='button' onclick=\"removeRow(this,'"+modId+"')\">-</button></td>"+
										"<td class='idx'>"+idx+"</td>"+
										"<td modUnitId='"+$(this).html()+"'>"+ $(this).html()+"</td>"+
										"</tr>";
										$("#tbody"+modId).append(tmp);
									});
									refreshUI(2,"tbody"+modId,false);
								}
							});
							
							refreshUI(1,"tbody"+tName,false);
							
							$(this).dialog("close");
						},"關閉" :function() {
							$(this).dialog("close");
						}
					}
				});
			}
		}, error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
}



/** 確定指定的子系統下，MR或OL，其SVN設定正確且連線成功 **/
//	0 尚未測試  1 沒有為此子系統設定 2 主機不能連 3 帳密不通  4 連線正常
function chkSVN4subsys(subsysId,svnType) {
	$.ajax({type:"post",url:"ChkSVN4subsys",async:false,dataType:"text",
		data : {subsysId:subsysId,svnType:svnType},
		success : function(reses) {
			$("#resSVN4subsys").val( parseInt(reses) );
		}, error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
}

/**
 *      @param code	SVN連線測試的代碼
 *      @returns {String}	翻譯的結果
 *		<br>創建時間：2013/10/2 下午3:06:39<br>
 *		創建人員：郭啟揚 (842297)
 */
function transSVNconnCode(code) {
	var res = "不明問題，請向系統管理員反應";
	
	if (code=="2") {
		res = "主機不能連";
	} else if (code=="3") {
		res = "帳密不通";
	} else if (code="4") {
		res = "連線正常";
	}
	return res;
}

/**
 *      @returns {String} 回傳目前時間
 *		<br>創建時間：2013/10/2 下午3:15:36<br>
 *		創建人員：郭啟揚 (842297)
 */
function currentTime() {
	var currentDate = new Date();
	return currentDate.getHours()+":"+currentDate.getMinutes()+":"+currentDate.getSeconds();
}

/**
 *      @returns {String} 回傳目前日期時間
 *		<br>創建時間：2013/10/2 下午3:14:00<br>
 *		創建人員：郭啟揚 (842297)
 */
function currentDateTime() {
	var currentDate = new Date();
	return currentDate.getFullYear()+"/"+(currentDate.getMonth()+1)+"/"+currentDate.getDate()+" "+currentDate.getHours()+":"+currentDate.getMinutes()+":"+currentDate.getSeconds();
}

/**
 *      @returns {String} 回傳目前日期
 *		<br>創建時間：2013/10/2 下午3:12:19<br>
 *		創建人員：郭啟揚 (842297)
 */
function currentDate() {
	var currentDate = new Date();
	return currentDate.getFullYear()+"/"+(currentDate.getMonth()+1)+"/"+currentDate.getDate();
}

/**
 *      確認SVN的連線是否正確
 *		@param url	SVN repo位址
 * 		@param usr	登入帳號
 * 		@param pwd	登入密碼
 * 		@returns	執行結果代號
 *		<br>創建時間：2013/10/2 下午3:07:01<br>
 *		創建人員：郭啟揚 (842297)
 */
function chkSVNconnection(url,usr,pwd) {
	var res = 0;
	$.ajax({type:"post",url:"ChkSVNconnection",async:false,dataType:"text",
		data : {url:url,usr:usr,pwd:pwd},
		success : function(reses) {
			res = reses;
		}, error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
	
	return res;
}

/** 清除所有網頁上的cookie **/
function clearALLcookie() {
	var cookies = $.cookie();
	for(var cookie in cookies) {
	   $.removeCookie(cookie);
	}
}

/** 將JSON陣列的值， **/
function insertSelect(selectId, jsonArrayObj) {
	$("#"+selectId+" option").remove();
	$.each(jsonArrayObj, function(i,res){
		var tmp = "<option value='"+res.s1+"'>"+res.s2+" ("+res.s1+")</option>";
		$("select#"+selectId).append(tmp);	
	});
}

/**	初始化畫面 **/
//pageName是該畫面的名稱
function init(pageName){
	
	if ( $.cookie("empId") == undefined && pageName != "index.html" ) {
		alert("請先進行登入");
		window.location = "index.html";
	}
	
	$("textarea").val("");
	
	// 加入 1. 長的錯誤訊息 2.windowFreeze 兩個對話窗
	$("html").append("<div id='errLongMsg'></div><div class='dialog' id='loading'><label for='imgLoading' class='eyecatch'> Loading...</label><br /><img id='imgLoading' src='pic/ajax-loader.gif'></div>");
	
	$(".dialog").css("visibility", "hidden");	//所有對話窗都先藏起來
	$.cookie("pageName", pageName );		//目前頁面的URL
	if ( pageName == "index.html" ) {
		document.title = "登入畫面";
		$("div#header").html( "<div><table id='banner'><tr><td rowspan='2' class='banner'><button id='btnBackToMain' type='button'>回<br><br>主<br><br>選<br><br>單</button></td><td class='banner'><h3 id='banner'>程式上線更版系統</h3></td><td rowspan='2'><img id='logout' src='pic/logout.png'></td></tr><tr><td id='actName' class='banner'>--登入畫面--</td></tr><tr class='right'><td id='user' colspan='3'>&nbsp;</td></tr></table></div><div id='outer2'><div id='sidebar2'><ul id='nav'></ul></div></div><p><p><hr><p><div id='errShortMsg' class='alert'></div><p>" );		
	} else {
		getInitInfo();
		document.title = $.cookie("pageTitle");	//頁面的中文名字
		$("div#header").html( genHeader() );		
	}
	
	$("table#banner button").button();
	$(".banner").prop("title","程式上線更版系統").bind("click",function(){ location.assign("devMenu.html"); });
	$("#logout").prop("title","登出系統").bind("click",function(){ logout(); location.assign("index.html"); });
	$("input[type='text']:first").focus();	//將游標移到第一個輸入欄位，讓使用者比較方便
}

/**	登出 **/
//將所有cookie都清除，並導向登入畫面
function logout(){
	clearALLcookie();
}

function getInitInfo(){
	$.ajax({type:"post",url:"QueryIniInfo",async:false,dataType:"json",
		data : {pageName:$.cookie("pageName"),empId:$.cookie("empId")},
		success : function(reses) {
			$.cookie("pageTitle",reses.s1);	//頁面抬頭
			$.cookie("empName",reses.s2);	//員工姓名
		}, error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});	
}


/**	產生色條 **/
//將指定tbody的body產生色條
//tbodyId是所指定的tbody的ID，isExistInd是，是否一併更新每筆record前的流水號
function refreshUI(typeId,tbodyId,isExistInd) {
	var oddColor = "#EAF4FD";
	var evenColor = "white";
	 
	if (typeId==2) {
		oddColor = "#D2E9FF";
	}
	
	if (isExistInd) {
		var num = $("#"+tbodyId+" > tr > td.idx").size();
		for (var i=0; i<num; i++) {	
			$("#"+tbodyId+" > tr > td.idx")[i].innerHTML=(i+1);
		}	
	}
	$("#"+tbodyId+" > tr:even").attr("bgcolor",evenColor);
	$("#"+tbodyId+" > tr:odd").attr("bgcolor",oddColor);
	$("#"+tbodyId+" button").button();
}

/**	啟動過場動畫 **/
function windowFreeze(str){
	str = (undefined==str)?"loading":str;
	$("#loading").css("visibility", "visible").dialog({
		height : 125,
		resizable : false,
		modal : true,
		closeOnEscape : false,
		draggable : false
	}).children("label").val(str).parent().prev().hide();
	// .parent().prev().hide() 是為了移除title bar用的
}

/**
 * 取得指定table的指定欄位值
 * @param tableName
 * @param columnName
 * @returns {String}
 */
function getTableColumnValue(tableName,columnName,constraint) {
	
	var tmp = "";
	$.ajax({type:"post",url:"GetTableColumnValue",dataType:"text",async:false,
		data:{tableName:tableName,columnName:columnName,constraint:constraint},
		success : function(res) {
			tmp = res;
		},error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
	
	return tmp;
}


/**	關閉過場動畫 **/
function windowUnfreeze(){
	$("#loading").dialog("close").css("visibility","hidden");
}

/** 表格Header的CheckBox **/
function chkALL(e) {
	var chkId = $(e).attr("id");
	var className = chkId.substring(0,chkId.length-3);
	if ($(e).is(":checked")) {
		$("." + className).each(function() {
			$(this).prop("checked", true);
		});
	} else {
		$("." + className).each(function() {
			$(this).prop("checked", false);
		});
	}
}

/** 每筆記錄前的CheckBox **/
function chk(className) {
	
	var isChecked = true;
	$("."+className).each(function(i){
		if (!$(this).is(":checked")) {
			isChecked=false;
		}
	});
	$("#"+className+"ALL").prop("checked", isChecked);
}

function sayHello() {
	var res = "哩厚";
	
	var currentDate = new Date();
	if ( currentDate.getHours() < 9 && currentDate.getHours() >= 2 ) {
		res = "早安";
	} else if ( currentDate.getHours() < 11 && currentDate.getHours() >= 9 ) {
		res = "早上好";
	} else if ( currentDate.getHours() < 13 && currentDate.getHours() >= 11 ) {
		res = "午安";
	} else if ( currentDate.getHours() < 18 && currentDate.getHours() >= 13 ) {
		res = "下午好";
	} else if ( currentDate.getHours() < 2 || currentDate.getHours() >= 18 ) {
		res = "晚安";
	}
	
	res += "~ &nbsp;";
	return res;
}
function thousandsSeprat(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

/*	產生每個頁面上方的區塊	*/
function genHeader() {
	return "<div><table id='banner'><tr><td rowspan='2' class='banner'><button id='btnBackToMain' type='button'>回<br><br>主<br><br>選<br><br>單</button></td><td class='banner'><h3 id='banner'>程式上線更版系統</h3></td><td rowspan='2'><img id='logout' src='pic/logout.png'></td></tr><tr><td id='actName' class='banner'>--"+$.cookie("pageTitle")+"--</td></tr><tr class='right'><td id='user' colspan='3'>"+sayHello()+$.cookie("empName")+"</td></tr></table></div><div id='outer2'><div id='sidebar2'><ul id='nav'></ul></div></div><p><p><hr><p><div id='errShortMsg' class='alert'></div><p>";
}

/**	移除表格中的一筆記錄
 *      @param obj
 *      @param tName ex: tableReq, tbodyReq,則tName即是Req
 *		<br>創建時間：2013/10/9 下午3:16:43<br>
 *		創建人員：郭啟揚 (842297)
 */
function removeRow(obj,tName) {
	var rNum = $(obj).parent().next().html();	//取相對位置，不將序號寫在按鈕內，因為好讀去，但無法在刪除時更新其它的，我們用下面addClass的方式解決這個問題
	$("tr.row"+tName+rNum).remove();
	refreshUI(1,"tbody"+tName,true);
	var idx=$("#tbody"+tName+" td.idx").size();
	
	for(var i=Number(rNum); i<=idx; i++ ) { //從被刪除的那列，往後開始異動每筆的ID等內容 (ex: 共5筆，刪了第2筆，則3,4,5 這三筆的內容，都要往前移)
		nextItemNum = (i+1);

		for(var a=1;a<4;a++){
			$("label[for='radio"+a+"_"+nextItemNum+"']").prop("for","radio"+a+"_"+i);
			$("#radio"+a+"_"+nextItemNum).prop("id","radio"+a+"_"+i);
		}
		
		$("input[name='radio"+nextItemNum+"']").prop("name","radio"+i);	//將下個radio的name，改為刪掉的這個
		
		$("tr.row"+tName+nextItemNum).addClass("row"+tName+i);		//下一筆的類別，多了現在被刪除這筆的類別
		$("tr.row"+tName+i).removeClass("row"+tName+nextItemNum);	//刪除下一筆原本既有的類別 
		
		$("div.radio"+nextItemNum ).addClass("radio"+i);
		$("div.radio"+i ).removeClass("radio"+nextItemNum);
	}
}

/**	取得字串中，括弧內的文字(前提是此字串僅能有一對括弧)
 *      @param str
 *      @returns
 *		<br>創建時間：2013/10/9 下午3:25:16<br>
 *		創建人員：郭啟揚 (842297)
 */
function getInPar(str) {
	return str.substring((str.indexOf("(")+1),(str.indexOf(")")));
}

/**
 * <自己加的，不是畫面出來預帶的>模組相依性中，單個模組的 + 鈕的事件
 * @param obj 那顆+號的按鈕
 */
function selDownstreamMod(obj) {
	var tName = $(obj).attr("modId"); 
	
	$.ajax({type:"post",url:"QueryJobOnJenkins",dataType:"json",async:false,
		data:{subsysId:$("select#subsysId option:selected").val()},
		success : function(reses) {
			if ( reses.length==0 ) {
				errorMsgShow("子系統"+subsysId+"未有任何模組被維護，請洽系統人員");
			} else {
				$("#tbodyModsPicker tr").remove();	//先將舊的記錄刪除
				$("#tableModsPickerChkALL").prop("checked",false);
					
				$.each(reses, function(i,res){
					if ( res != tName) {
						var tmp = "<tr>"+
						  "<td class='center'><input class='tableModsPickerChk' onclick=\"chk('tableModsPickerChk');\" type='checkbox' value='"+res+"' /></td>"+	//選擇的checkbox
						  "<td class='idx chkRow'>"+($("#tbodyModsPicker td.idx").size()+1)+"</td>"+		//流水號
						  "<td class='chkRow'>"+res+"</td>"+	//模組名稱
						  "</tr>";
						$("#tbodyModsPicker").append(tmp);
						
						//如果原本有選的話，這裡要為它打勾
						if ( $("#tbody"+tName+" td[modunitid='"+res+"']").size()!=0 ) {
							$("#tbodyModsPicker tr:last input[type='checkbox']").prop("checked", true);
						}						
					}
				});
				
				//綁定輔助點選
				$(".chkRow").bind("click",function(){
					$(this).parent("tr").children("td:first").children("input[type='checkbox']").click();
				});
				//檢查一下，如果所有子項都有勾，那chkAll也要勾
				chk("tableModsPickerChk");
				
				refreshUI(1,"tbodyModsPicker",false);
				
				$("#modsPicker").prop("title","選擇模組").css("visibility", "visible").dialog({
					resizable:true,modal:true,show:"fold",hide:"fold",width:"300",async:false,
					buttons : {
						"確定" : function() {
							/* 將checkbox有勾選的項目，加入列表 */
							$("#tbody"+tName+" tr").remove();	//將原本有選的刪除

							$(".tableModsPickerChk").each(function(i){
								if ($(this).is(":checked")) {
									var idx= $("#tbody"+tName+" > tr").size()+1;
								    var tmp = "<tr class='row"+tName+idx+"'>"+
									"<td class='center'><button class='hideable' type='button' onclick=\"removeRow(this,'"+tName+"')\">-</button></td>"+
									"<td class='idx'>"+idx+"</td>"+
									"<td modUnitId='"+$(this).val()+"'>"+ $(this).val()+"</td>"+
									"</tr>";
									$("#tbody"+tName).append(tmp);
								}
							});
							
							refreshUI(2,"tbody"+tName,false);
							
							$(this).dialog("close");
						},"關閉" :function() {
							$(this).dialog("close");
						}
					}
				});
			}
		}, error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
}

//產生子系統下的模組集合供選擇
function queryMods(subsysId){
	$.ajax({type:"post",url:"QueryModUnit",dataType:"json",async:false,
		data:{subsysId:subsysId,pageName:$.cookie("pageName")},
		success : function(reses) {
			if ( reses.length > 0 ) {
				$("#tbodyModsPicker tr").remove();
				
				var tmp="";
				var idx=0;
				
				$.each(reses, function(i, res){
					idx=($("#tbodyModsPicker td.idx").size()+1);
					tmp = "<tr>"+
					  "<td class='center'><input encap='"+res.s2+"' modId='"+res.s1+"' class='tableModsPickerChk' onclick=\"chk('tableModsPickerChk');\" type='checkbox'/></td>"+	//選擇的checkbox
					  "<td class='idx chkRow'>"+idx+"</td>"+		//流水號
					  "<td class='engN chkRow'>"+res.s1+"</td>"+	//模組名稱
					  "</tr>";
					$("#tbodyModsPicker").append(tmp);
					
					//如果原本有選的話，這裡要為它打勾
					if ( $("#tbodyMods > tr > td[modId='"+res.s1+"']").html()!=undefined ) {
						$("#tbodyModsPicker tr:last input[type='checkbox']").prop("checked", true);
					}
				});
				
				//綁定輔助點選
				$(".chkRow").bind("click",function(){
					$(this).parent("tr").children("td:first").children("input[type='checkbox']").click();
				});
				//檢查一下，如果所有子項都有勾，那chkAll也要勾
				chk("tableModsPickerChk");
				refreshUI(1,"tbodyModsPicker",false);
				
				$("#modsPicker").prop("title","選擇模組").css("visibility", "visible").dialog({
					resizable:true,modal:true,show:"fold",hide:"fold",width:"420",async:false,
					buttons : {
						"確定" : function() {
							/* 將checkbox有勾選的項目，加入列表 */
							
							$(".tableModsPickerChk").each(function(i){
								if ($(this).is(":checked")) {
									var modId = $(this).attr("modId");
									
									if( $("#tbodyMods td[modId='"+modId+"']").size()==0 ){
										//加入 已選擇模組的清單  tbodyMods
										idx= $("#tbodyMods > tr").size()+1;
									    tmp = "<tr class='rowMods"+idx+"'>"+
										"<td class='center'><button type='button' class='hideable' onclick=\"removeRow(this,'Mods')\">-</button></td>"+
										"<td class='idx'>"+idx+"</td>"+
										"<td class='engN' modId='"+modId+"'>"+ modId+"</td>"+
										"<td class='engN'>"+$(this).attr("encap")+"</td>"+
										"<td class='center'>"+
											"<button onclick='swit(this)' class='up' type='button'>▲</button>"+
											"<button onclick='swit(this)' class='down' type='button'>▼</button>"+
										"</td>"+
										"</tr>";
										$("#tbodyMods").append(tmp);
									}
								}
							});
							
							refreshUI(1,"tbodyMods",false);
							$(this).dialog("close");
						},"關閉" :function() {
							$(this).dialog("close");
						}
					}
				});
			}
		},error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
}




/**
 * 改變Row的順序
 * @param e
 */
function swit(e){

	var curRow = $(e).parent("td").parent("tr");
	
	if ( $(e).hasClass("up") ) {
		curRow.insertBefore( curRow.prev() );
	} else {
		curRow.insertAfter( curRow.next() );
	}
	
	refreshUI(1,"tbodyMods",true);
}


//由於進行jQuery運作時，ID中不能有.，所以必須進行下兩函數置換
function recoverDot(str){ 
	var find=escapeRegExp("_");
	var replace=escapeRegExp(".");
	return recoverSlash(str.replace(new RegExp(find, 'g'), replace));
}

function rep(str) {
	var tmp = str.replace(new RegExp("'", 'g'), "_");
	return tmp.replace(new RegExp("\"", 'g'), "_");
}

function replaceDot(str) {
	var find=escapeRegExp(".");
	var replace=escapeRegExp("_");
	return replaceSlash( str.replace(new RegExp(find, 'g'), replace) );
}

function recoverSlash(str) {
	var find=escapeRegExp(TOKENONE);
	var replace=escapeRegExp("/");
	return str.replace(new RegExp(find, 'g'), replace);
}

function replaceSlash(str) {
	var find=escapeRegExp("/");
	var replace=escapeRegExp(TOKENONE);
	return str.replace(new RegExp(find, 'g'), replace);
}

function escapeRegExp(str) {
  return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

/*function replaceDot(str){ 
	return str.replace( new RegExp ( ".","gm" ), "__");
}*/

//要為指定目標選擇需求
function addReq(tName,modId) {
	clearErrorMsg();
	windowFreeze();
	queryReqINsubsys( $("select#subsysId option:selected").val(),tName,modId);
	windowUnfreeze();
}

function addPerformer(tName) {
	clearErrorMsg();
	windowFreeze();
	queryPerformer( $("select#subsysId option:selected").val(),tName);
	windowUnfreeze();
}

function isProcess(str) {
	return (typeof str !== "undefined" && str != null && str!="" );
}

//是否為正整數(包含0)
function isPositiveNumaric(str) {
	var n = ~~Number(str);
    return String(n) === str && n >= 0;
}


/**
 * 檢查這些 加入、移出
 *	按鈕是否可以被按下
 */
function chkButtonClickAble() {
	if ( $("select#inPeople option").size() > 0 ) {	//存在被選中的人，所以可以全部移出
		$("#btnRemoveAll").removeAttr("disabled");
	} else {
		$("#btnRemoveAll").attr("disabled",true);
	}
	if ( $("select#notInPeople option").size() > 0 ) {	//存在未被選中的人，所以可以加入
		$("#btnAddAll").removeAttr("disabled");
	} else {
		$("#btnAddAll").attr("disabled",true);
	}
	var isSelected = $("#inPeople option:selected").size() > 0;
	if ( isSelected ) {
		$("#btnRemoveSel").removeAttr("disabled");	
	} else {
		$("#btnRemoveSel").attr("disabled",true);
	}
	isSelected = $("#notInPeople option:selected").size() > 0;
	if ( isSelected ) {
		$("#btnAddSel").removeAttr("disabled");	
	} else {
		$("#btnAddSel").attr("disabled",true);
	}
}


/**
 * 依據 角色及子系統，改變inPeople 與 notInPeople 的人員清單
 */ 
function chgPeoList() {
	if ( $("select#subsysId  option:selected").val() != "noselect" && $("select#role  option:selected").val() != "noselect" ) {
		
		var subsysId = $("select#subsysId  option:selected").val();	//子系統
		var roleId = $("select#role  option:selected").val();		//角色
		
		$.ajax({type:"post",url:"QueryEmpsWithRole",dataType: "json",
			data : {subsysId:subsysId ,roleId:roleId},
			success : function(reses) {
				$("select#notInPeople option").remove();
				$("select#inPeople option").remove();
				
				if ( $(reses.inList).size() >0 ) {
					$.each(reses.inList,function(i, res) {
						var tmp = "<option value='"+res.s1+"'>"+res.s2+"</option>";
						$("select#inPeople").append(tmp);
					});
				}
				if ( $(reses.outList).size() >0 ) {
					$.each(reses.outList,function(i, res) {
						var tmp = "<option value='"+res.s1+"'>"+res.s2+"</option>";
						$("select#notInPeople").append(tmp);
					});
				}
				chkButtonClickAble();
			},error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
		});
	}
}


/**	綁定 下拉吧 選擇的事件，參數url，為後端要呼叫的servlet名稱 **/ 
function selectSubSys(url) {
	
	var title = "資源群組";
	
	if ( url=="QueryEnv") {
		title="環境";
	} else if ( url=="QueryHost") {
		title="主機";
	} else if ( url=="QuerySquad") {
		title = "資源群組";
	} else if ( url=="QuerySqGroup") {
		$("#a_form #fc").val( "1" );
		title = "群組集合";
	} else if ( url=="QueryWCL") {
		$("#a_form #fc").val( "1" );
		title = "Web Console 清單";
	} else if ( url=="QueryEmps") {
		title = "人員";
	} else if ( url=="QueryReqJqGrid") {
		title = "需求";
	}
	
	$("#subsysId").bind("change", function(){
		var subsysId = $("select#subsysId option:selected").val();
		var subSysName = $("select#subsysId option:selected").text();
		
		jQuery("#gd").jqGrid('setGridParam',{
			url: url,
			datatype: "json",
			postData: {
				subsysId: function() { return subsysId; },
				fc: function() { return $("#a_form #fc").val(); }
			}
		}).jqGrid("setCaption","子系統："+subSysName+" 下轄"+title+"列表").trigger('reloadGrid');

		reconfigPermissions('1','gd');			
	});
}

/*	切換grid下方的  新增、修改、刪除	*/
function reconfigPermissions(isOpen,gd) {
    var ops = ['#add_'+gd, '#edit_'+gd, '#del_'+gd];
    $.each(ops, function (ix, value) {
        var $td = $(value);
        if (isOpen=='1') { $td.show(); }
        else { $td.hide(); }        
    });
}


/*	檢查是否要解決對子系統選項 的 鎖定 */
function chkIfLockSubSys(){
	if ( $("table#banner:contains('部署程式')").size() > 0 ) {	//dep 中的 手動式部署 要用
		if  ( $("table#fileReqMapping tr").size() < 3 ) {	//僅有標題和一支程式，或什麼都沒有，或畫面初始
			$("select#subsysId").prop("disabled",false);
		} else { $("select#subsysId").prop("disabled",true); }
	} else {	//depApply2要用
		if ( $("#removeCommHook").size()==0 && $("table#deliList").size()==0 ) {	//共用程式區空了，需求下轄程式也空了
			$("select#subsysId").prop("disabled",false);
		} else if ( $("#commFileList tr").size()==5 && $("table#deliList").size()==0 ) {		//共用程式僅剩一筆，需求下轄程式為空
			$("select#subsysId").prop("disabled",false);
		} else if ( $("#removeCommHook").size()==0 && $("table#deliList tr").size() > 1 ) {		//共用程式為空，需求下轄程式僅剩一筆
			$("select#subsysId").prop("disabled",false);
		} else {
			$("select#subsysId").prop("disabled",true);
		}	
	}
}


/**	呼叫輸入查詢SVN條件的畫面 **/
//這個函式可能被 1. 需求下轄程式  或 2. 共用程式 所驅動
function selectSVN(drivenBy) {
	width = 440;
	height = 260;
	
	//表示先前有查過
	if ( $("#tbodyFileSelectResult").length ) {
		width = 1000;
		height = 530;
		
		//需求下轄程式(req)，每筆record前面是用 checkbox，反之共用程式(common)，則是radio
		if ( drivenBy == "req" ) {	//需求下轄程式
			if ( $("#tbodyFileSelectResult:contains('input[type='radio']')") ) {
				$("#tbodyFileSelectResult tr td.chk input[type='radio']").each(function(){
					tmp= "<input class='svnFileSelect' fileName='"+$(this).attr("fileName")+"' SVNpath='"+$(this).attr("SVNpath")+"' verSet='"+$(this).attr("verSet")+"' selectedVer='"+$(this).attr("selectedVer")+"' reqId='"+$(this).attr("reqId")+"' source='"+$(this).attr("source")+"' type='checkbox' />";
					$(this).parent().html(tmp);
				});
				$("#fileSelectRemove thead th:first").html("<input id='tableFileSelectResultChkALL' onclick='chkALL(this);' type='checkbox' />");		//需求下轄程式的Header有 CheckALL 的選項
			}
		} else {	//共用程式
			if ( $("#tbodyFileSelectResult:contains('input[type='checkbox']')") ) {
				$("#tbodyFileSelectResult tr td.chk input[type='checkbox']").each(function(){
					tmp= "<input name='selectFile' fileName='"+$(this).attr("fileName")+"' SVNpath='"+$(this).attr("SVNpath")+"' verSet='"+$(this).attr("verSet")+"' selectedVer='"+$(this).attr("selectedVer")+"' reqId='"+$(this).attr("reqId")+"' source='"+$(this).attr("source")+"' type='radio' />";
					$(this).parent().html(tmp);
				});
				$("#fileSelectRemove thead th:first").html("");
			}
		}
	}
	
	$("#fileSelect #drivenBy").val(drivenBy);
	
	$("#fileSelect").css("visibility", "visible").dialog({
		resizable : false,
		width : width,
		height : height,
		show: "slide",
		hide: "slide",
		modal : true,
		buttons : {
			"關閉" : function() {
				if ( drivenBy == "req" ) {	//需求下轄程式
					$("input.svnFileSelect:checked").each( function(i){
						/*
						 * <<需求下轄程式區>>
						 */
						var fileName = $(this).attr("fileName");
						var SVNpath = $(this).attr("SVNpath");
						var verSet = $(this).attr("verSet");
						var selectedVer = $(this).attr("selectedVer");
						var reqId = $(this).attr("reqId");
						var source = $(this).attr("source");
						var selectFileSize = $("#fileReqMapping tr").size();
						
						var fileDepType = "n";	//預設是以non-web的方式進行部署
						var fileType = "<select fc='underReq' onchange='selFileDepType(this)'><option>non-web";
						var Readonly = "";
						if ( isEndsWith(fileName,"__war") || isEndsWith(fileName,"__ear") ) {
							fileType += "</option><option selected='selected'>web";
							Readonly ="Readonly";
							fileDepType = "w";
						} else {
							fileDepType = "n";
						}
						
						fileType += "</option></select>";
						
						tmp = "<tr>"+
						"<td class='center'><button type='button'>-</button></td>"+		//刪除該列的按鈕
						"<td class='center'>"+selectFileSize+"</td>"+																			//流水號
						"<td class='content'>"+fileName.replace( new RegExp ( "__" , "gm" ), ".")+"</td>"+										//檔名
						"<td class='right' id='"+selectFileSize+"'><select verSet='"+verSet+"' id='"+SVNpath+"/"+fileName+"'></select></td>"+	//版次選擇
						"<td class='content'>"+fileType+"</td>"+																				//Web or non-Web
						"<td class='content'>"+SVNpath+"</td>"+																					//SVN路徑
						"<td><input type='text' id='dep_"+SVNpath+"/"+fileName+selectFileSize+"' Readonly="+Readonly+" value=''></td>"+			//部署路徑
						"<td><button type='button'>＊</button></td>"+					//資源群組的選擇
						"<td><ul class='squad "+reqId+"' id='ul_"+SVNpath+"/"+fileName+selectFileSize+"'></ul></td>"+							//資源群組的顯示
						"</tr>";
						$("#fileReqMapping").append(tmp);
						
						$("#fileReqMapping tr:last button:first").bind("click", function() {
							removeTar(this,'fileReqMapping',source);
						});
						
						/*	將 找尋 squad 或 squadGroup的功能，綁給*那個按鈕	*/
						$("#fileReqMapping tr:last button:last").attr("fileDepType",fileDepType).bind("click",function() {	//因為剛剛append，一定是最後一筆 tr
							if (source=='1') {	//來自交付那個書面		
								selDepConf(this ,'3', $(this).attr("fileDepType"));
				 			} else {
								selDepConf(this ,'4', $(this).attr("fileDepType"));
				 			}
						});
						
						if ( fileDepType=='w' ) {
							$("#fileReqMapping tr:last input[type='text']").attr("readonly",true);	//若為web程式，將部署路徑的input 設為不可輸入
						} else {
							$("#fileReqMapping tr:last input[type='text']").attr("readonly",false);	//若為non-web程式，將部署路徑的input 設為可輸入
						}
						
						refreshUI(1,"tbodyFileSelectResult",false);
						
						if (source==2) { chkIfLockSubSys(); }	//若是手動部署，要對子系統的選擇項進行管控
						
						tmp= "fileReqMapping td#"+selectFileSize+" select";
						genOption(verSet,selectedVer,tmp);
					});
				} else {
					$("input[name='selectFile']:checked").each( function(i){
						var fileName = $(this).attr("fileName");
						var SVNpath = $(this).attr("SVNpath");
						var verSet = $(this).attr("verSet");
						var selectedVer = $(this).attr("selectedVer");
						
						/** 
						 * 雙擊了SVN列出的檔案清單 
						 * <<共用程式區>>
						 **/
						$(".cf").attr("disabled",false);	//選擇檔案後，將檔案的編輯功能予以enable
						$("#fileSelect").dialog("close");	//關掉選擇檔案的 對話框
						$("#commSelName").val( fileName.replace( new RegExp ( "__" , "gm" ), ".") );
						$("#commSVNPath").html( SVNpath );
						$("#commVerSet").val(verSet);		//為了修改時不用再次查詢SVN，將這個檔案先前查的版本存起來
						
						$("#commExeWay option").remove();
						$("#commExeWay").append("<option>non-web</option>");
						$("#commExeWay").attr("disabled",false);
						
						if ( isEndsWith(fileName,"__war") || isEndsWith(fileName,"__ear") ) {
							$("#commDepPath").attr("readonly",true);
							$("#commExeWay").append("<option selected='selected'>web</option>");
							$("#commSelSqu").bind("click",function() {
								selDepConf(this,'1','w');	
							});
						} else {
							$("#commSelSqu").bind("click",function() {
								selDepConf(this,'1','n');	
							});
						}
						
						if ( !$("#tableReqList").length >0 ) {	
							tmp = "<div id='forRemoveReqList'><hr/><table id='tableReqList'><thead class='tableFirst'><tr><th><button type='button' onclick=\"queryReqWindowOpen('underFile')\">+</button></th><th>＃</th><th>需求編號</th><th>需求名稱</th></tr></thead><tbody id='tbodyReqList'></tbody></table></div>";
							$("#reqList").append(tmp);
							
						}
						tmp = "commFileVer";
						
						genOption(verSet,selectedVer,tmp);
					});
				}
				$("button").button();
				$(this).dialog("close");
			}
		}
	});
}

/**
 * 按下查詢SVN後，產生SVN檔案列表
 * 參數說明：
 *	tar：表示這些找到的資料，要產生在那個div中
 *	source：在那個畫面驅動這個method
 *		1 --> 發起自「交付」
 *		2 --> 發起自「手動式部署」
 **/
function genSvnList(tar,source) {
	$("#"+tar).hide();
	tmp = "<img id='svnLoading' alt='讀取中...' src='pic/ajax-loader.gif'>";
	$("#fileSelectResult").append(tmp);
	/*僅需求下轄程式才有*/
	var reqId = $("#reqSelectedId").val();
	$("#fileSelectRemove").remove();
	$.post("QuerySVNdepAble",{ fn:$("#fileName_q").val(),subsysId:$("select#subsysId option:selected").val() },function(reses) {
		$("#svnLoading").remove();
		if (reses.length > 0) {
			
			//若是需求下轄程式，則產生 Checkbox，若是 共用程式區，則用 Radio Button
			if ( $("#fileSelect #drivenBy").val() == "req" ) {
				tmp= "<input id='tableFileSelectResultChkALL' onclick='chkALL(this);' type='checkbox' />";
			} else {
				tmp = "";
			}
			
			tmp = "<div id='fileSelectRemove'><hr/><table id='tableFileSelectResult'><thead class='tableFirst'><tr><td colspan='5'>※勾選欲選擇的檔案</td></tr><tr><th class='center'>"+tmp+"</th><th>＃</th><th>部署檔名稱</th><th class='center'>版次</th><th>產生日期</th><th>路徑</th></tr></thead><tbody id='tbodyFileSelectResult'></tbody></table></div>";
			$("#fileSelectResult").append(tmp);
			if ( $("#fileSelect #drivenBy").val() == "req" ) {
				$("#fileSelectRemove thead input:first").bind("click", function(){
					chkALL("svnFileSelect");	
				});
			}
			
			$.each(reses,function(i, res) {
				var id = res.id;
				var verSet = "";
				$.each(res.verSet, function(i, ver) {
					verSet += (ver+",");
				});
				//若是需求下轄程式，則產生 Checkbox，若是 共用程式區，則用 Radio Button
				if ( $("#fileSelect #drivenBy").val() == "req" ) {
					tmp= "<input class='svnFileSelect chkRow tableFileSelectResultChk' fileName='"+id+"' SVNpath='"+res.home+"' verSet='"+verSet+"' selectedVer='"+res.ver+"' reqId='"+reqId+"' source='"+source+"' type='checkbox' />";
				} else {
					tmp= "<input fileName='"+id+"' SVNpath='"+res.home+"' verSet='"+verSet+"' selectedVer='"+res.ver+"' reqId='"+reqId+"' source='"+source+"' type='radio' name='selectFile' />";
				}
				tmp = "<tr class='engN'><td class='center'>"+ tmp +"</td><td class='int chkRow'>"+(i+1)+"</td><td class='chkRow'>"+id.replace( new RegExp ( "__" , "gm" ), ".")+"</td><td class='int chkRow'>"+res.ver+"</td><td class='chkRow'>"+res.lastUpdateDate+"</td><td class='chkRow'>"+res.home+"</td></tr>";
				$("#tbodyFileSelectResult").append(tmp);
			});
			
			//綁定輔助點選
			$("#tbodyFileSelectResult .chkRow").bind("click",function(){
				$(this).parent("tr").children("td:first").children("input[type='radio']").click();
			});

			refreshUI(1,"tbodyFileSelectResult",false)
			
			$("#fileSelect").dialog("option","width", 1000 ).dialog("option","height", 530 ).dialog({ position: { my: "center", at: "center", of: window } });
		} else {
			tmp = "<div id='fileSelectRemove' class='alert'><hr/>查無符合條件的檔案.</div>";
			$("#fileSelectResult").append(tmp);
			$("#fileSelect").dialog("option","width", 440 ).dialog("option","height", 260 ).dialog({ position: { my: "center", at: "center", of: window } });
		}
		$("#"+tar).show();
	},"JSON");
	$("#fileSelectResult").css("visibility", "visible");	
}

/*	判斷字串 str，是否以 suffix 結尾	*/
function isEndsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

/* 增加版次旳選項 
 * 
 * 參數說明：
 * 		 verSet：自SVN查回來，該支檔案的版本變動記錄	
 * 	selectedVer：下拉bar中，被選擇所需要的版本
 * 		 selTar：SELECT 的 ID
 * */
function genOption(verSet,selectedVer,selTar) {
	$("#"+selTar+" option").remove();
	var n = verSet.split(",");
	for(var i=0;i<n.length-1;i++) {
		var SELECTED = "";
		if (n[i]==selectedVer) { SELECTED = " SELECTED"; }
		$("#"+selTar).append("<option "+ SELECTED +" >"+n[i]+"</option>");
	}
}

/** 
 * 	按下＊，要來選擇部署對象的設定
 * 		non-web 指定 squad
 * 		web 指定 squadGroup
 * 
 * 	參數說明：
 * 		obj	：	按下的那button的參照
 * 	procType：	執行動作的分類
 * 			1 -->	發起自 共用程式
 * 			2 -->	發起自 重新部署
 * 			3 -->	發起自 需求下轄程式
 * 			4 -->	發起自 手動部署
 * 	fileDepType	：	程式部署的方式
 * 			n -->	non-web
 * 			w -->	web
 *  **/
function selDepConf(obj,procType,fileDepType){
	/** 到後端讀取可以勾選的選項 **/
	var subsysId = $("select#subsysId option:selected").val();
	var tmp ="";
	
	if ( procType == "2" ) {
		subsysId = $("#reDepConfirmSingle select#selSub option:checked").val();
		$("#squadSelect select option:first").html( $("#reDepConfirmSingle select#selSub option:checked").html() );
	}
	
	$.post("QuerySubsysSquad",{ subsysId:subsysId,fileDepType:'n' },function(reses) {
		$("#tbodySquadSelect tr").remove();
		$.each(reses,function(i, res) {
			var checked = "";
			
			if (procType == "4"|| procType == "3") {	//3,交付下的需求 或 4,手動式部署
				$(obj).parent().next().children("ul").children("li").each(function(i,str) {
					if (str.innerHTML==(res.id+"-"+res.name)) { checked = "checked"; }
				});	
			} else if ( procType == "2" ) {	//單檔重新部署
				$("#reDepConfirmSingle ul li").each(function(i,str) {
					if (str.innerHTML==res.name) { checked = "checked"; }
				});
			} else if ( procType == "1" ) {	//共用檔案
				$("#commonFileSelect ul").children("li").each(function(i,str) {
					if (str.innerHTML==(res.id+"-"+res.name)) { checked = "checked"; }
				});	
			}
			
			if (procType == "2") {
				tmp = "<tr><td class='center'><input type='checkbox' squadPath='"+res.squadPath+"' subsysId='"+res.id+"' squad='"+res.name+"' class='subsysSquad chkRow tableSquadSelectChk' "+checked+"></td><td class='squ chkRow'>"+res.name+"</td></tr>";
			} else {
				tmp = "<tr><td class='center'><input type='checkbox' squadPath='"+res.squadPath+"' subsysId='"+res.id+"' squad='"+res.name+"' class='subsysSquad chkRow tableSquadSelectChk' "+checked+"></td><td class='squ chkRow'>"+res.name+"</td></tr>";				
			}
			$("#tbodySquadSelect").append(tmp);
		});
		
		$("#tbodySquadSelect .chkRow").bind("click",function(){
			$(this).parent("tr").children("td:first").children("input[type='checkbox']").click();
			chk("tableSquadSelectChk");
		});
		
		refreshUI(1,"tbodySquadSelect",false);
	},"JSON");	
	
	$("div#divWCLSel table#wcl").remove();
	if ( fileDepType == 'w' ) {
		
		
		$.post("QuerySqGroup",{ fc:"3",bssId:"MBMS", subsysId:subsysId },function(reses) {
				if( reses.length > 0  ) {
					$("#squadSelect div#divWCLSel").append("<table id='wcl'><thead class='tableFirst'><th>&nbsp;</th><th>群組集合</th></tr></thead></table><p>");
					
					$.each( reses, function(i, res) {
						if ( res.id !=0 ) {	//0是無設定，故略過
							tmp = "<tr><td class='center'><input type='checkbox' sqGroup='"+res.squadPath+"'></td><td>"+res.name+" ("+res.id+")</td></tr>";
							$("table#wcl").append(tmp);	
						}
					});	
					$("table#wcl input[type='checkbox'").bind("click",function(){
						var isChecked = $(this).is(":checked");
						tmp = $( this ).attr("sqGroup").split("UUUU");
						for ( var i=0; i<tmp.length;i++ ) {
							$("#tbodySquadSelect td.squ").each( function() {
								if ( $(this).html().indexOf( "("+tmp[i]+")") !== -1 ) {
									$(this).prev().prev().children("input[type='checkbox']").prop("checked", isChecked );	
								}
							});
						}
					});
				}
				
		});
	}
	
	if ( procType == "2" ) { tmp = 200; } else {tmp=400;}
	$("#squadSelect").css("visibility", "visible").dialog({
		width : tmp,
		height : 350,
		show: "slide",
		hide: "slide",
		modal : true,
		buttons : {
			"關閉" : function() {	//將畫面目前的勾選狀態寫回
				sq = [];
				if (procType == "3" || procType == "4") {
					$(obj).parent().next().children("ul").children("li").remove();	
				} else if (procType == "2") {
					$("#reDepConfirmSingle ul li").remove();	
				} else if (procType == "1") {
					$("#commonFileSelect ul").children("li").remove();	
				}
				
				$(".subsysSquad").each(function(i,res) {
					var isChecked = $(this).prop("checked");
					if (isChecked) {
						var sqid = $(res).attr("squad");
						var subsysId = $(res).attr("subsysId");
						var squadPath = $(res).attr("squadPath");
						sq.push({subsysId:getInPar(subsysId),sqid:getInPar(sqid)});
						if (procType == "3") {
							$(obj).parent().next().children("ul").append("<li>"+subsysId+"-"+sqid+"</li>");
						} else if (procType == "2") {
							$("#reDepConfirmSingle ul").append("<li>"+sqid+"</li>");
						} else if (procType == "1") {
							$("#commonFileSelect ul").append("<li>"+subsysId+"-"+sqid+"</li>");
						} else if (procType == "4") {
							$(obj).parent().next().children("ul").append("<li sp='"+squadPath+"'>"+subsysId+"-"+sqid+"</li>");
						}
					}
				});
				
				if (sq.length > 0) {
					if (procType != "2") {
						$("#sqCheck").val(JSON.stringify(sq));
						if (procType == "3" || procType == "4") {
							if ( $(obj).parent().prev().children("input").val() == "" ) {	//如果沒有輸入，就幫使用者查詢既有存在的
								$.post("QuQueryPa", {squadCount:sq.length,sqCheck:$("#sqCheck").val(),fn: $(obj).parent().prev().prev().prev().prev().prev().html() } ,function(res) {
									$(obj).parent().prev().children("input").val(res);
								},"TEXT");	
							}	
						}  else if (procType == "1") {
							if ( $("#commDepPath").val() == "" ) {	//如果沒有輸入，就幫使用者查詢既有存在的
								$.post("QueryPath", {squadCount:sq.length,sqCheck:$("#sqCheck").val(),fn: $("#commSelName").val()} ,function(res) {
									$("#commDepPath").val(res);
								},"TEXT");	
							}
						}
					}
				}
				if ( procType == "4") { chkReady(); }	//手動式部署需要檢查
				
				$("#squadSelect").css("visibility", "hidden");
				$(this).dialog("close");
			}
		}
	});
}


/*	目前選擇的程式，進行web, non-web的切換	*/
function selFileDepType(obj) {
	fDT = "n";
	if ( $(obj).val() == "web" ) {
		fDT="w";
		if ( $(obj).attr("fc") == "underReq" ) {
			$(obj).parent().next().next().children("input[type='text']").attr("readonly",true).val("").parent().next().children("button").attr("fileDepType",fDT);
			$(obj).parent().next().next().next().next().children("ul").children("li").remove();
			$(obj).parent().next().next().next().children("button").unbind().bind("click",function(){ selDepConf(this,'3',fDT); });
		} else if ( $(obj).attr("fc") == "commonFile" ) {
			$(obj).parent().parent().next().next().children("td").children("input[type='text']").attr("readonly",true).val("").parent().prev().prev().children("button").attr("fileDepType",fDT);
			$("#commSquLists ul li").remove();
			$("#commSelSqu").unbind().bind("click",function() { selDepConf(this,'1',fDT); });	//按下* 時，改查詢sqGroup
		}
	} else {
		fDT="n";
		if ( $(obj).attr("fc") == "underReq" ) {
			$(obj).parent().next().next().children("input[type='text']").attr("readonly",false).parent().next().children("button").attr("fileDepType",fDT);
			$(obj).parent().next().next().next().next().children("ul").children("li").remove();
			$(obj).parent().next().next().next().children("button").unbind().bind("click",function(){ selDepConf(this,'3',fDT); });
		} else if ( $(obj).attr("fc") == "commonFile" ) {
			$(obj).parent().parent().next().next().children("td").children("input[type='text']").attr("readonly",false).parent().prev().prev().children("button").attr("fileDepType",fDT);
			$("#commSquLists ul li").remove();
			$("#commSelSqu").unbind().bind("click",function() { selDepConf(this,'1',fDT); });	//按下* 時，改查詢sqGroup
		}
	}
}

/**	只能輸入數字
 * @param e
 * @param pnumber
 * @returns {Boolean}
 */
function validateNumber(e, pnumber)
{
	if (!/^\d+$/.test(pnumber)) {
		var newValue =/^\d+/.exec(e.value);         
		if (newValue != null) {             
        	e.value = newValue;
    	} else {          
        	e.value = "";    
    	} 
    }
	
    return false;
}

/*
 * 計算目前手邊檔案 變數filePortfolio，
 * web, nonweb的組合，
 * 並會回傳計算的結果
 * 
 * 參數inc，代表包含的類型
 * ex: maniFilePortfolio("web")
 * 則若目前的filePortfolio值
 * 為1，則不變，回傳1
 * 為2，將其改為1，回傳1
 * 若3，則不變，回傳3
 * 	
 * 1為有web和nonweb，2為僅有nonweb，3為僅有web
 * 
 * */
function maniFilePortfolio( inc ) {
	if ( $("#filePortfolio").val()=="1"  ) {
		return 1;
	} else if ( $("#filePortfolio").val()=="2" ) {
		if ( inc=="web" ) {
			$("#filePortfolio").val("1");
			return 1;
		} else {
			return 2;
		}
	} else if ( $("#filePortfolio").val()=="3" ) {
		if ( inc=="web" ) {
			return 3;
		} else {
			$("#filePortfolio").val("1");
			return 1;
		}
	} else {
		if ( inc=="web" ) {
			$("#filePortfolio").val("3");
			return 3;
		} else {
			$("#filePortfolio").val("2");
			return 2;
		}
	}
}

/** 取得目前的時間 */
function cdt() {
	var today = new Date();
	var ss = today.getSeconds();
	var HH = today.getHours();
	var min = today.getMinutes();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd;} if(mm<10){mm='0'+mm;} if(HH<10){HH='0'+HH;} if(ss<10){ss='0'+ss;} if(min<10){min='0'+min;}
	today = yyyy+'/'+mm+'/'+dd+" &nbsp; "+HH+":"+min+":"+ss;
	return today;
}

/** 取得指定系統參數 */
function getSysParameter(paraName) {
	var pv = "";
	$.ajax({type:"post",url:"GetSecificSysParameter",dataType:"text",async:false,
		data:{paraName:paraName},
		success : function(res) {
			pv= res;
		},error : function(xhr, ajaxOptions, thrownError) { errorAjx(xhr); }
	});
	return pv;
	
}


/**	設定畫面欄位比例值
 * @param initialValue
 */
function setMainDivWidth(initialValue) {
	var screenWidth = screen.width;
	
	var perInterval = (100-initialValue) / 7;
	if( screenWidth == 1920 )  {
		$("div#mainDiv").width(initialValue+"%");
	} else if( screenWidth == 1776 )  {
	 	$("div#mainDiv").width((initialValue+perInterval)+"%");
	} else if( screenWidth == 1680 )  {
	 	$("div#mainDiv").width((initialValue+2*perInterval)+"%");
	} else if( screenWidth == 1440 )  {
	 	$("div#mainDiv").width((initialValue+3*perInterval)+"%");
	} else if( screenWidth == 1280 )  {
	 	$("div#mainDiv").width((initialValue+4*perInterval)+"%");
	} else if( screenWidth == 1152 )  {
	 	$("div#mainDiv").width((initialValue+5*perInterval)+"%");
	} else if( screenWidth == 1024 )  {
	 	$("div#mainDiv").width((initialValue+6*perInterval)+"%");
	} else {
		$("div#mainDiv").width("100%");
	}
	
	console.log("目前的螢幕寬："+screenWidth+", 設定的比例為："+$("div#mainDiv").width());
}

/**	檢查是否為IP **/
function checkIP(ip){
	var re = /^((\d)|(([1-9])\d)|(1\d\d)|(2(([0-4]\d)|5([0-5]))))\.((\d)|(([1-9])\d)|(1\d\d)|(2(([0-4]\d)|5([0-5]))))\.((\d)|(([1-9])\d)|(1\d\d)|(2(([0-4]\d)|5([0-5]))))\.((\d)|(([1-9])\d)|(1\d\d)|(2(([0-4]\d)|5([0-5]))))$/;
	return re.test(ip)?true:false; 
}

/** 取得指定Grid，目前選擇row的指定欄位 
 *	grid	要操作的 table
 *	col	 	要取的cell在colModel中的名稱
 *	inPar	boolean值，有些欄位是如  訂單(OR), 為true時，則回傳OR，即括號中的值
 */
function getColVal(grid,col,inPar) {
	var sel_id = $("#"+grid).jqGrid("getGridParam", "selrow");
	var res = "";
	
	if (inPar) {
		var tmpStr = $("#"+grid).jqGrid("getCell", sel_id, col);
		res = getInPar(tmpStr);
	} else {
		res = $("#"+grid).jqGrid("getCell", sel_id, col);
	}
	return res;
}


function checkBox(e) {
	e = e||event;/* get IE event ( not passed ) */ 
	e.stopPropagation? e.stopPropagation() : e.cancelBubble = true;
	
	var isChecked = $("input[type='checkbox']:first").is(":checked");

	if (isChecked) {
		$("input[type='checkbox']").prop("checked", true);
	} else {
		$("input[type='checkbox']").prop("checked", false);
	}
}

//取得系統參數
function getSystemParameter(parameterName) {
	
}